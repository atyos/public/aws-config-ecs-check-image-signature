require 'digest'
require 'rake/clean'
require 'time'
require 'zip'

# Global variables
GO_MODULE="gitlab.com/atyos/public/aws-config-ecs-check-image-signature"
GO_APP_NAME="aws-config-ecs-check-image-signature"
GO_APPS_TARGET_ARCHS=[
    {:os => 'linux', :arch => 'amd64'},
#    {:os => 'linux', :arch => 'arm64'},
#    {:os => 'darwin', :arch => 'amd64'}
]
BUILD_DIR="build"

# Clean build directory
CLEAN.include "#{BUILD_DIR}/**/*"

# Format the source code
task :format do
    sh "go fmt ./...", verbose: false
end

# Lint the source code
task :lint do
    sh "golangci-lint run", verbose: false
end

# Run unit tests
namespace "test" do
    task :run do
        sh "go test ./...", verbose: false
    end

    task :runv do
        sh "go test -v ./...", verbose: false
    end

    task :coverage => ['clean'] do
        sh "
            mkdir -p #{BUILD_DIR}/test-coverage
            go test ./... -cover -coverprofile=#{BUILD_DIR}/test-coverage/report.out
            go tool cover -html=#{BUILD_DIR}/test-coverage/report.out -o #{BUILD_DIR}/test-coverage/report.html 
        "
    end
end
task :test => ["test:run"]

# Run godoc serve
namespace "docs" do
    task :serve do
        sh "
            echo Open browser at: http://localhost:6060/pkg/#{GO_MODULE}
            godoc -http=:6060
        ", verbose: false
    end
end

# Build binaries
desc 'build'
task :build, [:version] => ['clean'] do |t, args|
    args.with_defaults(:version => "__UNSET__")

    GO_APPS_TARGET_ARCHS.each do |target_arch|
        # log
        puts "build: #{GO_APP_NAME}:#{target_arch[:os]}:#{target_arch[:arch]}"

        # compile opts: output
        output_bin = "#{BUILD_DIR}/bin/#{target_arch[:os]}/#{target_arch[:arch]}/#{GO_APP_NAME}"

        # compile opts: ldflags.
        ldflags="
            -s -w
            -extldflags \"-static\"
        "
        
        # compile opts: tags.
        tags = "netgo"

        # build: go binary
        sh "
            export GOOS=#{target_arch[:os]}
            export GOARCH=#{target_arch[:arch]} 
            export CGO_ENABLED=0
            go build -tags #{tags} -ldflags=\"#{ldflags}\" -o #{output_bin} .
        ", verbose: false

        # compute: md5
        open("#{output_bin}.md5", 'w') do |f|
            f.puts Digest::MD5.hexdigest File.read output_bin
        end

        # compute: sha256
        open("#{output_bin}.sha256", 'w') do |f|
            f.puts Digest::SHA256.hexdigest File.read output_bin
        end

        # generate zip file
        Zip::File.open("#{output_bin}.zip", create: true) do |zipfile|
            zipfile.add("main", output_bin)
            zipfile.add("#{GO_APP_NAME}", output_bin)
            zipfile.add("#{GO_APP_NAME}.md5", "#{output_bin}.md5")
            zipfile.add("#{GO_APP_NAME}.sha256", "#{output_bin}.sha256")
        end
    end
end