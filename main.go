package main

import (
	"context"
	"encoding/json"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/configservice"
)

func HandleRequest(ctx context.Context, event AWSConfigEvent) (string, error) {
	// Configure logging
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Get config from env
	kmsKeyArn := os.Getenv("COSIGN_AWS_KMS_KEY_ARN")
	awsConfigTestMode, err := strconv.ParseBool(os.Getenv("AWS_CONFIG_TEST_MODE"))
	if err != nil {
		log.Fatalf("an error occurred during signature check: %v", err)
	}

	// Deserialize AWS COnfig event
	var configInvokingEvent AWSConfigInvokingEvent
	json.Unmarshal([]byte(event.InvokingEvent), &configInvokingEvent)

	// Parse ECS task definition
	ecsTaskDefinitionArn := configInvokingEvent.AWSConfigurationItemChangeNotificationEvent.Arn
	ecsTaskDefinitionConfig := configInvokingEvent.AWSConfigurationItemChangeNotificationEvent.AWSECSTaskDefinitionConfig

	ecsTaskDefinitionCaptureTime, err := time.Parse(time.RFC3339Nano, configInvokingEvent.AWSConfigurationItemChangeNotificationEvent.ConfigurationItemCaptureTime)
	if err != nil {
		log.Fatalf("fail to parse aws config time: %v", err)
	}

	ecsTaskCompliance := "COMPLIANT"

	// Check each image present in the ECS Task definition and determine compliance
	log.Debugf("analyzing docker images of ECS task with ARN %s", ecsTaskDefinitionArn)
	for _, ecsContainerDefinition := range ecsTaskDefinitionConfig.ContainerDefinitions {
		log.Debugf("> container %s uses image %s", ecsContainerDefinition.Name, ecsContainerDefinition.Image)

		ecsContainerImageSignatureValid, err := VerifySignature(ctx, kmsKeyArn, ecsContainerDefinition.Image)
		if err != nil {
			log.Fatalf("an error occurred during signature check: %v", err)
		}

		if !ecsContainerImageSignatureValid {
			log.Warnf("ECS task %s / container %s uses a non-signed image.", ecsTaskDefinitionArn, ecsContainerDefinition.Name)
			ecsTaskCompliance = "NON_COMPLIANT"
		}
	}

	// Push back status to AWS Config
	awsSession := session.Must(session.NewSession())
	awsConfigService := configservice.New(awsSession)
	input := &configservice.PutEvaluationsInput{
		Evaluations: []*configservice.Evaluation{
			{
				ComplianceResourceId:   &configInvokingEvent.AWSConfigurationItemChangeNotificationEvent.ResourceId,
				ComplianceResourceType: &configInvokingEvent.AWSConfigurationItemChangeNotificationEvent.ResourceType,
				ComplianceType:         &ecsTaskCompliance,
				OrderingTimestamp:      &ecsTaskDefinitionCaptureTime,
			},
		},
		ResultToken: &event.ResultToken,
		TestMode:    &awsConfigTestMode,
	}
	log.Debugf("putting results into AWS Config: %s", input.String())
	putEvaluationsOutput, err := awsConfigService.PutEvaluations(input)

	if err != nil {
		log.Fatalf("an error occurred during aws config put evaluations: %v", err)
	} else {
		log.Infof("result: %s", putEvaluationsOutput.String())
	}

	return "OK", nil
}

func main() {
	lambda.Start(HandleRequest)
}
