package main

import (
	"context"
	"fmt"

	ecrlogin "github.com/awslabs/amazon-ecr-credential-helper/ecr-login"
	"github.com/awslabs/amazon-ecr-credential-helper/ecr-login/api"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote"
	"github.com/sigstore/cosign/pkg/cosign"
	ociremote "github.com/sigstore/cosign/pkg/oci/remote"
	sigs "github.com/sigstore/cosign/pkg/signature"
	log "github.com/sirupsen/logrus"
)

func VerifySignature(ctx context.Context, kmsKeyArn string, containerImage string) (bool, error) {
	log.Infof("using KMS key to verify signature: %s", kmsKeyArn)
	pubKey, err := sigs.LoadPublicKey(ctx, fmt.Sprintf("awskms:///%s", kmsKeyArn))
	if err != nil {
		log.Error("fail to load KMS key: %v", err)
		return false, err
	}

	log.Infof("parsing ref of %s", containerImage)

	// Get container from ECR
	ref, err := name.ParseReference(containerImage)
	if err != nil {
		return false, err
	}

	log.Debugf("building ECR helper to access image")

	ecrHelper := ecrlogin.NewECRHelper(ecrlogin.WithClientFactory(api.DefaultClientFactory{}))
	opts := []remote.Option{
		remote.WithAuthFromKeychain(authn.NewKeychainFromHelper(ecrHelper)),
		remote.WithContext(ctx),
	}

	// Build a CheckOpts cosign object
	co := &cosign.CheckOpts{
		ClaimVerifier:      cosign.SimpleClaimVerifier,
		RegistryClientOpts: []ociremote.Option{ociremote.WithRemoteOptions(opts...)},
		SigVerifier:        pubKey,
	}

	// Verify Image
	log.Info("performing signature check")
	verifiedSigs, _, err := cosign.VerifyImageSignatures(ctx, ref, co)
	if err != nil {
		log.Errorf("signature check failed: %v", err)
		return false, nil
	}

	return len(verifiedSigs) > 0, nil
}
