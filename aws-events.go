package main

// The AWS event emitted by Config and sent to the Lambda.
type AWSConfigEvent struct {
	InvokingEvent  string `json:"invokingEvent"`
	ResultToken    string `json:"resultToken"`
	ConfigRuleId   string `json:"configRuleId"`
	ConfigRuleName string `json:"configRuleName"`
}

// The invokingEvent emitted by Config.
type AWSConfigInvokingEvent struct {
	AWSConfigurationItemChangeNotificationEvent struct {
		Arn                          string `json:"ARN"`
		ResourceId                   string `json:"resourceId"`
		ResourceType                 string `json:"resourceType"`
		ConfigurationItemCaptureTime string `json:"configurationItemCaptureTime"`
		AWSECSTaskDefinitionConfig   struct {
			ContainerDefinitions []struct {
				Name  string `json:"Name"`
				Image string `json:"Image"`
			} `json:"ContainerDefinitions"`
		} `json:"configuration"`
	} `json:"configurationItem"`
}
