# Verify signature compliance of your ECS images with AWS Config and Lambda

## Description

This project is a lambda aiming at checking the compliance of Docker images used within ECS.

The prerequisites are the following :
* Docker images are pushed to AWS ECR
* Docker images are signed using Sigstore and a AWS KMS keys

## How to set-up

### Create an IAM role for the Lambda

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CreateLogGroup",
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:<region>:<account_id>:*"
        },
        {
            "Sid": "PutLogEvents",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:<region>:<account_id>:log-group:/aws/lambda/<lambda_name>:*"
        },
        {
            "Sid": "PutEvaluationsToConfig",
            "Effect": "Allow",
            "Action": "config:PutEvaluations",
            "Resource": "*"
        },
        {
            "Sid": "GetKMSPublicKey",
            "Effect": "Allow",
            "Action": [
                "kms:GetPublicKey",
                "kms:DescribeKey"
            ],
            "Resource": "<KMS_KEY_ARN>"
        },
        {
            "Sid": "GetTokenForECR",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken"
            ],
            "Resource": "*"
        },
        {
            "Sid": "GetImageFromECR",
            "Effect": "Allow",
            "Action": [
                "ecr:BatchGetImage",
                "ecr:GetDownloadUrlForLayer"
            ],
            "Resource": "<ECR_REPOSITORY_ARN> (arn:aws:ecr:<region>:<account_id>:repository/<repository_name>)"
        }
    ]
}
```

### Build and create Lambda in AWS

```bash
$ rake build
```

The script will produce a deployable zip package in `build/bin/linux/amd64/aws-config-ecs-check-image-signature.zip`

### Create a Lambda in AWS

Proceed to the creation of the Lambda in AWS with the following configuration:

```bash
$ aws lambda create-function --function-name <lambda_name> \
    --zip-file fileb://build/bin/linux/amd64/aws-config-ecs-check-image-signature.zip \
    --handler main \
    --runtime go1.x --architectures x86_64 \
    --environment Variables={AWS_CONFIG_TEST_MODE=true|false,COSIGN_AWS_KMS_KEY_ARN=<KMS_KEY_ARN>} \
    --role <lambda_execution_role_arn>
```

where:
- AWS_CONFIG_TEST_MODE: `true` if you want to test integration with AWS Config without raising false-positive alerts, `false` to run in production.
- COSIGN_AWS_KMS_KEY_ARN: the ARN of the KMS key ID which should be used to verify the image.